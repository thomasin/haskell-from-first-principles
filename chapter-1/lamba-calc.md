# Lambda calculus
#### Equivalence exercises
1. `𝜆xy.xz` = `𝜆mn.mz`
2. `𝜆xy.xxy` = `𝜆x.(𝜆y.xxy)` = `𝜆a.(𝜆b.aab)`
3. `𝜆xyz.zx` = `𝜆x.(𝜆y.(𝜆z.zx))` = `𝜆tos.st`

#### Chapter exercises
**Combinators**
Determine if each of the following are combinators or not. 
1. `𝜆x.xxx` — Yes
2. `𝜆xy.zx` — No
3. `𝜆xyz.xy(zx)` — Yes
4. `𝜆xyz.xy(zxy)` — Yes
5. `𝜆xy.xy(zxy)` — No

**Normal form or diverge?**
Determine if each of the following can be reduced to a normal form or if they diverge. 
1. `𝜆x.xxx` — Normal
2. `(𝜆z.zz)(𝜆y.yy)` — Divergent 
3. `(𝜆x.xxx)z` — Normal

**Beta reduce**
Evaluate (that is, beta reduce) each of the following expressions to normal form. We *strongly*recommend writing out the steps on paper with a pencil or pen. 
1. `(𝜆abc.cba)zz(𝜆wv.w)`
  * = `(𝜆a.(𝜆b.(𝜆c.cba)))zz(𝜆w.(𝜆v.w))`
  * = `(𝜆|a := z|.(𝜆b.(𝜆c.cba)))z(𝜆w.(𝜆v.w))`
  * = `(𝜆b.(𝜆c.cbz))z(𝜆w.(𝜆v.w))`
  * = `(𝜆|b := z|.(𝜆c.cbz))(𝜆w.(𝜆v.w))`
  * = `(𝜆c.czz)(𝜆w.(𝜆v.w))`
  * = `𝜆|c := (𝜆w.(𝜆v.w))|.czz`
  * = `(𝜆w.(𝜆v.w))zz`
  * = `(𝜆|w := z|.(𝜆v.w))z`
  * = `(𝜆v.z)z`
  * = `𝜆|v := z|.z`
  * = `z`
2. `(𝜆x.𝜆y.xyy)(𝜆a.a)b`
  * = `(𝜆|x := (𝜆a.a)|.𝜆y.xyy)b`
  * = `(𝜆y.(𝜆a.a)yy)b`
  * = `𝜆|y := b|.(𝜆a.a)yy`
  * = `(𝜆a.a)bb`
  * = `(𝜆|a := b|.a)b`
  * = `bb`
3. `(𝜆y.y)(𝜆x.xx)(𝜆z.zq)`
  * = `(𝜆|y := (𝜆x.xx)|.y)(𝜆z.zq)`
  * = `(𝜆x.xx)(𝜆z.zq)`
  * = `𝜆|x := (𝜆z.zq)|.xx`
  * = `(𝜆z.zq)(𝜆z.zq)`
  * = `(𝜆|z := (𝜆z.zq)|.zq)`
  * = `(𝜆z.zq)q`
  * = `𝜆|z := q|.zq`
  * = `qq`
4. `(𝜆z.z)(𝜆z.zz)(𝜆z.zy)`
  * = `(𝜆x.x)(𝜆m.mm)(𝜆z.zy)`
  * = `(𝜆|x := (𝜆m.mm)|.x)(𝜆z.zy)`
  * = `(𝜆m.mm)(𝜆z.zy)`
  * = `𝜆|m := (𝜆z.zy)|.mm`
  * = `(𝜆z.zy)(𝜆z.zy)`
  * = `𝜆|z := (𝜆z.zy)|.zy`
  * = `(𝜆z.zy)y`
  * = `𝜆|z := y|.zy`
  * = `yy`
5. `(𝜆x.𝜆y.xyy)(𝜆y.y)y`
  * = `(𝜆x.𝜆y.xyy)(𝜆m.m)y`
  * = `(𝜆|x := (𝜆m.m)|.𝜆y.xyy)y𝑦`
  * = `(𝜆y.(𝜆m.m)yy)y𝑦`
  * = `(𝜆y.(𝜆|m := y|.m)y𝑦)y`
  * = `(𝜆y.yy)y𝑦`
  * = `𝜆|y := y|.yy𝑦`
  * = `yy`
6. `(𝜆a.aa)(𝜆b.ba)c`
  * = `(𝜆x.xx)(𝜆b.ba)c`
  * = `(𝜆|x := (𝜆b.ba)|.xx)c`
  * = `(𝜆b.ba)(𝜆b.ba)c`
  * = `(𝜆|b := (𝜆b.ba)|.ba)c`
  * = `(𝜆b.ba)ac`
  * = `(𝜆|b := a|.ba)c`
  * = `aac`
7. `(𝜆xyz.xz(yz))(𝜆x.z)(𝜆x.a)`
  * = `(𝜆xyz.xz(yz))(𝜆m.n)(𝜆b.a)`
  * = `(𝜆x.(𝜆y.(𝜆z.xz(yz))))(𝜆m.n)(𝜆b.a)`
  * = `(𝜆|x := (𝜆m.n)|.(𝜆y.(𝜆z.xz(yz))))(𝜆b.a)`
  * = `(𝜆y.(𝜆z.(𝜆m.n)z(yz)))(𝜆b.a)`
  * = `𝜆|y := (𝜆b.a)|.(𝜆z.(𝜆m.n)z(yz))`
  * = `𝜆z.(𝜆m.n)z((𝜆b.a)z)`
  * = `𝜆z.(𝜆|m := z|.n)((𝜆b.a)z)`
  * = `𝜆z.n((𝜆b.a)z)`
  * = `𝜆z.n(𝜆|b := z|.a)`
  * = `𝜆z.na`
