# Comprehension check

1. Given the following lines of code as they might appear in a source file:
  `half x = x / 2` and `square x = x * x`
  Write the same declarations in your REPL and then use the functions half and square in some experimental expressions.

2. Write one function that has one parameter and works for all the following expressions. Be sure to name the function.
  * `3.14 * (5 * 5)`
  * `3.14 * (10 * 10)`
  * `3.14 * (2 * 2)`
  * `3.14 * (4 * 4)`

  = `multiplyByPi x = 3.14 * x`

3. There is a value in Prelude called pi. Rewrite your function to use pi instead of 3.14.
  = `multiplyByPi x = pi * x`


# Parentheses and Association
Below are some pairs of functions that are alike except for parenthesization. Read them carefully and decide if the parentheses change the results of the function. Check your work in GHCi.

1. a) `8 + 7 * 9`
   b) `(8 + 7) * 9`
   Yes - the parentheses wrap around a lower association

2. a) `perimeter x y = (x * 2) + (y * 2)`
   b) `perimeter x y = x * 2 + y * 2`
   No - the parentheses wrap around the highest associations

3. a) `f x = x / 2 + 9`
   b) `f x = x / (2 + 9)`
   Yes - the parentheses wrap around a lower association

# Heal the Sick
The following code samples are broken and won’t compile. The first two are as you might enter into the REPL; the third is from a source file. Find the mistakes and fix them so that they will.

1. `area x = 3. 14 * (x * x)`
   = `area x = 3.14 * (x * x)`
2. `double x = b * 2`
   = `double x = x * 2`
3. see `heal-the-sick.hs`

# A Head Code
Now for some exercises. First, determine in your head what the following expressions will return, then validate in the REPL:
These examples are prefixed with let because they are not dec- larations, they are expressions.

1. `let x = 5 in x` = `5`
2. `let x = 5 in x * x` = `25`
3. `let x = 5; y = 6 in x * y` = `30`
4. `let x = 3; y = 1000 in x + 3` `6`

